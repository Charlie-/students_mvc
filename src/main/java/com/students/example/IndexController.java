package com.students.example;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.students.example.Student;

/**
 * Handles requests for the application home page.
 */
@Controller
public class IndexController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "/getstudents", method = RequestMethod.GET)	
	public @ResponseBody List<Student> getStudents(){
		return studentList;
	}
	
	@RequestMapping(value = "/removestudent/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Student> removeStudent(@PathVariable int id){
		studentList.remove(id);
		
		return studentList;
	}
	
	@RequestMapping(value = "/addstudents", method = RequestMethod.GET)
	public @ResponseBody List<Student> readdStudents(){
		studentList = getList();
		return studentList;
	}
	
	public List<Student> studentList = getList();
	
	
	private List<Student> getList(){
		List<Student> studentList = new ArrayList<Student>();
		studentList.add(new Student("Bruce", "Wayne", "5/5/1944"));
		studentList.add(new Student("Clark", "Kent", "6/12/1955"));
		studentList.add(new Student("Bob", "Barker", "2/1/1988"));
		studentList.add(new Student("Oprah", "Winfrey", "1/1/1921"));
		
		return studentList;
	}
	
}
