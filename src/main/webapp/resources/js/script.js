$(function(){
	getStudents();
	removeStudents();
	reAddStudents();
});

function getStudents(){
	$.ajax({
		method: 'GET',
		url: '/getstudents'
	}).done(function(response){
		$('.loader-container').hide();
		updateTable(response);
	});
}

function removeStudents(){
	$('body').on('click', '.s-row', function(){
		var studentId = $(this).attr('id');
		$.ajax({
			method: 'GET',
			url: '/removestudent/' + studentId
		}).done(function(response){
			updateTable(response);
		});
	});
}

function reAddStudents(){
	$('body').on('click', '.empty-container', function(){
		$.ajax({
			method: 'GET',
			url: '/addstudents'
		}).done(function(response){
			$('.loader-container').hide();
			updateTable(response);
		});
	});
}

function updateTable(rows){
	$('.empty-container').hide();
	$('.student-rows').empty();
	
	if(rows.length === 0){
		$('.empty-container').show();
	}else{
		for(var i = 0; i < rows.length; i++){
			$('.student-rows').append('<div class="s-row" id="'+ i +'"><div class="s-col">'+ rows[i].firstName + '</div><div class="s-col">'+ rows[i].lastName + '</div><div class="s-col s-col-right">'+rows[i].birthday + '</div></div>');
		}
	}
}