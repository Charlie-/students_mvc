<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Students List</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
	<script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
	<script src="<c:url value="/resources/js/script.js"/>"></script>
</head>	
<body>
	<div class="students-container">
		<div class="s-head">
			<div class="s-col">First Name</div>
			<div class="s-col">Last Name</div>
			<div class="s-col s-col-right">Birthday</div>
		</div>
		<div class="loader-container">
			<img src="<c:url value="/resources/loader.gif"/>"/>
		</div>
		<div class="empty-container">
			There are currently no students in the database.<br>
			Click here to re-add the students.
		</div>
		<div class="student-rows">
		</div>
	</div>
</body>
</html>